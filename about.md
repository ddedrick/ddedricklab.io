---
layout: page
title: About
permalink: /about/
---

Back in 2015 I decided it would be beneficial to document especially
challenging debugging issues for future reference. I've come back to these
logs on many occassions to recall debugging techniques to be applied to new
issues and also to answer questions I've been asked about issues that
happened in the past. I'm now sharing these logs in case they end up being
useful to any one else.
