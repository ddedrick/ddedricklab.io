---
layout: post
title: "DRAM corruption"
comments: true
---
A coworker discovered that after the DRAM speed was changed from 667MHz to 800MHz on
a new set of cards some set of the cards started failing in systemd on soft reboots. This
provided an interesting problem because at the point we were failing almost all
of the system, including abrt, has been shutdown.


The first thing I needed to do was to find a way to get a coredump out of
systemd. When systemd's pid 1 gets a fatal signal it forks and kills the child
with that signal instead. This prevents pid 1 from going away and should allow
for a core dump to be generated. Unfortunately on our system in this scenario
the core dump fails. Luckily systemd provides an option that can help here. The
option is CrashShell=yes in system.conf. This will spawn a shell after systemd
has attempted to fork and kill the process. With this enabled and a card that
could reproduce this issue I was able to get a shell with systemd in the state
where it crashed. Now all I had to do was remount our writeable partition and run gdb against
pid 1 using the gcore command to save a core dump to /var/fs.


Now with a core dump I had something to start looking at. So the first thing I
see is that we are trying to jump to an area that is in the heap. The heap isn't
executable so we crash. The code that was trying to jump to the heap is hashmap
code using a function pointer. Taking a look at the hashmap code shows that
this function pointer is only ever set to 1 of about 3 or 4 values all of which
are clearly in the .text section. So the code shouldn't ever set the function
pointer to the value that it was set to. It took me far too long to realize from
this that the logical conclusion was that if a valid hashmap never had the value
that we saw then maybe the hashmap itself had been free and the memory
overwritten by something else.


To add extra complexity to all of this the hashmap, that I later discovered had
been freed, was actually allocated as part of a systemd pool. The relevant piece
of how systemd pools work here is that they have a pointer to free entries. The
first word at that entry is then a pointer to the next free entry and so on.
Why this is interesting is that the first word of the hashmap is the function
pointer. So when a hashmap is freed the function pointer will get overwritten
with a pointer to another free hashmap on the heap. Understanding all of this I
can see that this hashmap was freed.


When I take a look at the next step back I see the hashmap we are accessing is
part of a systemd Unit structure. The structure seemed semi-valid, some fields
made perfect sense while others were off by a bit. The hashmap in question is one
of the dependency hashmaps pointing to other units that have a dependency
interaction with this one. Now after further investigation into the data in the
structure that doesn't make sense I start to realize that this structure has
also been freed.


Take another step back. So who is accessing the freed Unit? Now I can see that
the freed Unit is being accessed by another Unit that is in the process of being
freed itself. It is going through all of its dependencies and removing itself
from them so that they won't point back to a freed Unit (preventing the very
problem we seem to be experiencing). Now how did the already freed Unit not
remove itself from this unit when it was going through and being freed.


As an aside here there were several other secarios that occurred that almost all
came down to this common point but they all shared having issues with hashmaps.


To get this information I decided to add debug to log information about when
units got freed. Several iterations in and I had pretty much all the debug I
needed to get a deeper understanding of what was happening. What I saw was that
we had in fact freed the Unit in question and we even ran the code to remove the
unit from the one that later has a reference back to it. However I could now
tell that the code that was removing somehow missed one entry.


At this point I had to stop and read up on exactly how systemd's hashmaps
worked. Here is some relevant info on it:
[http://codecapsule.com/2013/11/11/robin-hood-hashing/](http://codecapsule.com/2013/11/11/robin-hood-hashing)
. It's called Robin Hood hashing. The basic idea is that it trys to put the entry
in the hash bucket that it belongs in. If something is already there it will
attempt to shift the adjacent entries around such that the distance from the
appropriate hash bucket is as evenly spread out as possible. For a much better
description see the page that I referenced. The result is that for
any entry that is not in it's appropriate bucket, in other words any non-empty
entry that has a non-zero DIB, the entry to the left should be >= DIB+1. If
that's not the case the entry is in an invalid location. When I look at the DIB
entry for the item that we failed to remove from the hash table I find that it
is in fact in an invalid location. What I found was that the DIB was consistently
 0x1 or 0x2 and the entry to the left was freed.


Almost there now. At this point since the failures were so consistent I thought
there must surely be a bug in systemd's hashing code and I was about to find it.
Well visual inspection didn't find it on the first pass so time to add more
debug. I decided to add a check after every modification to the hashmap to check
the contents to make sure all of the DIBS were valid and then abort(). The
result was that I crashed a whole lot earlier than expected. I crash on early
boot when it was previously failing on shutdown. So things got
messed up early but had few reprecussions until we start to tear things down.

Now I've got a core dump from almost exactly when this occurred. Time to just
dig in and see what I've got. But wait when I fire up gdb on the core dump and
read the DIB value that I printed out it looks sane now and doesn't match what
my debug says. Certainly I must be doing something wrong so I added more debug
that would print the whole DIB table after it hit a failure. Well now I see the
same results my code catches a failure and then loops through and reads it again
and now the value is back to correct. Systemd's pid 1 is single threaded so
there shouldn't be anything else outside of my code that could be modifying this
and now I'm certain that my code isn't modifying this.


Obviously from the start I thought this was a DRAM issue but I wanted to get
evidence that was the case so that I could clear systemd. As I got deeper though
it became so consistent that I started to think maybe I was wrong and we were
chasing a systemd issue. Now I've come full circle and I can see that 2 reads,
with no writes inbetween, result in different values. This points to either DRAM
or cache issue.


At this point systemd can reproduce this fairly easily (almost every boot). My
code can actually catch failures that systemd might miss. A read failure once
might just mean that we miss starting a dependency but something else would
probably cause it to start anyway but my test would catch that. The failure that
systemd was catching was likely a complication of a read failure. For example
there are 3 consecutive entries with the following DIBS: 0x0, 0x1, 0x2. Now you
remove the entry with the 0x0 DIB and it will scan forward until it fings 0x0 or
0xff. However if you get a read failure on 0x2 and instead read 0x0 then it will
remove the entry at 0x0 and shift 0x1 to 0x0 but not shift 0x2 to 0x1 like it
should have.


Knowing that this was likely a DRAM issue I ran a memory test we already had to see if it could
catch anything. Unfortunately it didn't but when I went and wrote my own tester
for this scenario it did. The tester I wrote copied the data from a hashmap that
had seen a failure and I would constantly read and validate the data in 2 copies
of this hashmap (one in .rodata and the other in the heap). I also needed to put
some other stress on the system, which in the end I did by having 2 threads that
would constantly fork and exec ```pwd``` redirected to /dev/null. In order to
simplify the testing even further I modified my test to run in the bootloader so
that it could easily run with cache disabled and without any other interruptions
that might make it harder to track from a hardware perspective.


Now with a reproducible, provably hardware issue I could pass it off to the
approriate team to drive the investigation. From the hardware side they tried
several things but eventually the change that seemed to make it go away was
changing some of the DRAM tuning values.
