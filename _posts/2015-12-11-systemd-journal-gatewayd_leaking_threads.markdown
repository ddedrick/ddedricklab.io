---
layout: post
title: "systemd-journal-gatewayd leaking threads"
comments: true
---
Devices in testing started crashing with SIGSEGVs in systemd-journal-gatewayd. When
looking at the crash it showed it in libmicrohttpd calling ```pthread_setname_np()```
with a NULL pointer. The null pointer it was passing was the result of a failed
```pthread_create()``` call. Looking at the mapping showed that the reason for the
pthread_create failure had been an over abundance of threads that had exhausted
the mapping and left no room for an additional thread stack to be added.


With GDB I could see that all of the other threads are sitting waiting for more
journal data. The output of ```netstat -an``` indicates that there were a large
number of connections to the journal-gatewayd all in the CLOSE_WAIT state
(exactly as many as there were threads). This indicated that the client closed
the connection but systemd-journal-gatewayd was still holding onto them.


What was happening was that the threads were sitting waiting on more data from the
journal indefinitely and never checking the status of the connection. So as long
as there were no new journal messages every thread would continue to stick
around. To fix this I added a timeout to the journal wait of 10 seconds and if
we ever hit this timeout we jump back out to microhttpd who will do some
checking on the connection and if it was closed exit the thread, if it was still
open it was just jump back down into the wait again.

Fixes submitted upstream:

[https://github.com/systemd/systemd/pull/2144](https://github.com/systemd/systemd/pull/2144)

[https://lists.gnu.org/archive/html/libmicrohttpd/2016-03/msg00031.html](https://lists.gnu.org/archive/html/libmicrohttpd/2016-03/msg00031.html)t
