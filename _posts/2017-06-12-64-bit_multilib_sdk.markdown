---
layout: post
title: "64-bit multilib sdk"
comments: true
---
An external vendor reported an issue with building 64-bit code with the sdk.
They provided a simple c++ test program that did some basic file stream
operations. Specifically it opened a file, then looped on eof while doing a
getline. The issue they reported was that the test program looped forever after
it got to the end of the file. Upon compiling the program from an sdk myself I
found the same issue.


I did some research and it seemed that it was advised not to use ```.eof()``` to test
that getline had reached the end of the file. I'm not sure at this point how
useful that advice is but if I restructure the test code to react to the return
code of getline the issue goes away. However I decided to retest the original
code against 32-bit arm. I did this originally using a 32-bit only sdk and
found that it did not exhibit this issue. Additionally I used the 32-bit arm
compiler in the 64-bit sdk and was able to prove the same thing. This indicates
that the 64-bit version at least behaved differently than the 32-bit version.


So I broke out gdb and started poking around at the internals of the
```eof()``` and ```getline()``` calls. I found that in getline I could see it set the
internal eof flag but when we get to eof it doesn't see that flag as being set.
```eof()``` and ```getline()``` however are methods provided by different classes. ```eof()``` is
provided by basic_ios and ```getline()``` is provided by basic_istream, which inherits
basic_ios. With this information I tried to determine if ```eof()``` and ```readline()```
where referencing the same internal data. I did this by getting the offset of
common data between the 2 in each function. What I found was that the data was
0x10 bytes off. Since the size of the object was greater than 0x10 bytes this
indicated that there was some bad offset.


Knowing that I was looking for a bad offset I looked in depth at the
assembly code that was generated for the small test program. From this I
discovered that ```eof()``` was being called at an offset that was 0x10 off of what I
manually calculated. I was able to test this theory by manually rewriting the
offset instruction in the ELF file and then running the resulting file. The
result was that it did not exhibit the bad behavior. So what I know at this
point is that the compiler seemingly put the wrong offset in for the class. I
started poking around at gcc to try to figure out where this might be happening
in the compiler but with little luck.


It then occurred to me that I should see if the same results would occur when
building with bitbake instead of the sdk. What I found was that the output of
bitbake was correct, while the sdk was not. I decided to try
building with the different combinations of compilers and sysroots to see if I
could narrow down that failure. What I found was that any combination that used
the sdk sysroot failed and all the others passed. So something must be wrong
with the sdk sysroot. In order to track things down further I decided to run gcc
under strace and look at all of the files that were open and use that list of
files as a basis to diff between sdk sysroot and bitbake sysroot. When I did
this I immediately found that bits/wordsize.h was setting word size to 32 in the
sdk and 64 in bitbake.


So the fundamental issue was that the sdk and the installed libstdc++.so didn't
agree on the word size. Looking deeper the reason for this was that the sdk
sysroot was a combination of both arm and aarch64 packages. The result was that
we had a race when we generated the sdk to install the glibc header files. When
the 32-bit versions won then the only code that could be generated correctly was
32-bit but if the 64-bit version won then only 64-bit code could be built.


As a temprorary measure I have been handing out 64-bit only sdks. The long term
solution is much more challenging. It may involved modifying the glibc headers
to be multilib aware like they are on x86_64.


Update: This was actually fixed with a newer version of yocto. The fix was that
conflicts are no longer allowed in the sdk. This forced the issue and it seems
that glibc modified the aarch64 header files to have switches for 32-bit
versions. The result is that arm and aarch64 can happily build out of the same
tree.
