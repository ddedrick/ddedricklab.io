---
layout: post
title: "tbstack stopping processes"
comments: true
---
An issue was logged for a hang, which was traced back to a process not responding to
a request over IPC. A coworker noticed that the process was in the stopped state. To get into
that state someone must have sent SIGSTOP to the process. A search through
the logs shows that the last thing the process did was 10 minutes previous and appears
to be a debug collection. From this I was able to go back and find the output
of that collection. The result was that I noticed that the processes stack trace in
that collection got cut off but it was not yet stopped when it occurred. With
this I surmised that the most likely scenario was that when collecting stack
traces timed out it left the process in the stopped state. Upon digging in I found
that tbstack had a signal handler that would have gotten called by the SIGTERM
from the timeout. However the signal handler called some non-signal handler safe
functions (```ptrace()``` and ```exit()```).


To test this out I wrote a loop that would run the stack trace collection, which in turn calls
tbstack, in a loop under a timeout command that would always kill it
prematurely. Running this loop resulted in a failure every time in under 300
iterations. Then I modified the signal handler to remove the ptrace calls and
replace the ```exit()``` with ```_exit()```. The ptrace calls were not required since ptrace
will automatically detach on a process exitting. However the test revealed there
were still issues with that fix. A simple test program showed that if a process
is ptrace attached and then exits without doing a ptrace detach it is no longer
traced but it is left in the stopped state. Therefore I just needed to replace
the ptrace detach that was originaly present with sending a SIGCONT to the
process. This fixed the problem and never produced an error in 73000 iterations
of the test loop.

Upstream Fix:

[https://github.com/tbricks/tbstack/pull/4](https://github.com/tbricks/tbstack/pull/4)

Signal Safe calls:

[man signal-safety](http://man7.org/linux/man-pages/man7/signal-safety.7.html)
