---
layout: post
title: "aarch64 prelinking"
comments: true
---
In the process of upgrading poky we encountered a run time issues with
aarch64 where as soon as the kernel tried to exec initramfs it would crash.
The issue manifested as a ```unhandled level 1 translation fault (11)```. This
was problematic to debug because the issues seemed to occur as the very
first thing that was run in userspace so we couldn't get a shell to poke
around at the failure and get more debug. Some initial testing by Wes showed
that if prelinking was turned off the issue would go away and we could boot
to ready just fine.


The first logical place to try to debug was the exec path from the kernel up
through the dynamic linker (ld.so). In order to do this I attempted to set
LD_DEBUG=all on the kernel command line so it would be in the environment of
the first thing executed by the kernel. This however didn't yield any new
results. This indicated that either I wasn't doing it right or the issue was
occurring before ld.so got to any LD_DEBUG statements. In order to
investigate this I added some debug printfs to ld.so to try to track down if
we were even getting to that. I discovered that we were in fact getting to
ld.so, which indicated that we got past the kernel portion of exec.


I added more debug statements to ld.so and discovered that we were
progressing some way into ld.so but not to the point where we would actually
start the executable itself. This indicated that ld.so itself was crashing
and not busybox, the executable that was supposed to be started. Adding debug
statements here was a tedious process because they involved rebuilding glibc
and everything between it and a minimal image, which takes a significant
amount of time.


At this point, knowing that the issue was likely in ld.so, I decided to try
to reproduce this issue in qemu by simply running busybox. This proved to
fail and it failed with a SIGSEGV, which indicated that I was getting the
same issue as seen on hardware. This greatly simplified things because I
could reproduce the issue on the initramfs image, which required much less
building than the lowlevel image. I was also able to swap out the ld.so I
used with a non-prelinked one and confirm that it appeared to work just
fine. With this and printf statements I started narrowing down on where we
were actually failing.


Now I realized with qemu I realized that I could actually hook up gdb and
use it to debug the issue. I took a diversion down an incorrect debugging
path when I incorrectly loaded the symbols for ld.so. Using add-symbol-file
I was adding the ld.so symbols but I was loading them with the address of
offset 0 of the file in mind. What I failed to remember was that gdb loads
symbols files at the .text address. After this unfortunate diversion I was
then able to load the symbols correctly and start chasing the real issue.


With a proper gdb setup I started narrowing on the real issue, which
appeared to be that we were trying to relocate the .rela section to an
improper offset. Tracing this back it found that we were adding an offset on
top of the proper location that it should have been mapped to. The reason
for this was that ld.so was calculating the load offset by subtracing the
stored location of the .dynamic section with the actual offset. The stored
location of the .dynamic section, stored in the .got section, was not
adjusted for prelinking. So the root of the issue was that prelinking wasn't
properly fixing up this address.


Knowing the source of the issue I took a look at the aarch64 prelinking
source. I discovered that it had logic for finding the dynamic pointer in
the .got section but that it wasn't working. The issue was that it found the
.got.plt section, went back one, then fixed up the first entry. This was
broken because there was no .got.plt section. In addition to this the
dynamic pointer was no longer the first entry in the .got section.


Not knowing off the top of my head why we were missing the .got.plt section
I decided to remove linker flags one by one from the link command for ld.so
to see if one caused the .got.plt section to return. With this I discovered
that -Wl,-z,now was the cause of this change. Poky upstream had added the
--enable-bind-now flag to glibc as part of a commit to add hardening
configure options.


For now the solution is to disable the bind-now option for aarch64 but in
the long term the prelinking code should be reworked to properly handle elf
files linked with the bind-now option. The challenge is that the dynamic
pointer is no longer the first entry in the .got section. One experiment
left is to compare how this works on arm since it seems to work fine with
the bind-now option.


Update: I was able to fix this in binutils by making adjustments to the .got
and .got.plt layouts but I have yet to upstream my fixes.


Useful command:
~~~ bash
qemu-aarch64 -g 12345 -L ./ -E "LD_LIBRARY_PATH=./lib64:./usr/lib64" ./bin/busybox.nosuid
# When run from a rootfs this will execute aarch64 code and wait on port
# 12345 for gdb
~~~
