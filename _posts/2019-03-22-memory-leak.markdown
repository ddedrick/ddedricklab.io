---
layout: post
title: "Memory leak"
---
A very slow memory leak on our IPC server process was sent my way to look at. The
issue was showing up on multiple machines which appeared to be leaking at a few K
per hour. After a few weeks of uptime some specific machines would start to
suffer due to the issue but if they were rebooted or flashed it would reset the
memory back down to zero to start over again.


A quick test of my local machine on the exact same code didn't reproduce the
issue but luckily I found a few machines that were actively leaking. Testing
being run on these machines was stopped and I confirmed that the leak was still
occurring. This allowed me to attach gdb to the leaking process and pull
consecutive coredumps with increasing memory usage.


The coredump in gdb isn't especially helpful because I can't really tell who is
leaking just from the coredump. What I could do is dump the heap and then build
up some tool that would allow me to look for patterns in allocations in the
heap to help indentify where the source of the leak might be. Luckily gdb allows
for dumping memory to files. I was able to do so with this gdb command:
```dump memory /home/ddedrick/debug/leak/heap.1 0x0025000 0x0080000```


Dealing with the raw heap isn't especially helpful even after looking at the
hexdump since it's hard to tell what is actually allocated and what is a remnant
of freed memory. Luckily glibc provides some useful information in the heap that
can let us known what is allocated and what is freed. In the glibc malloc source
[here](https://sourceware.org/git/?p=glibc.git;a=blob;f=malloc/malloc.c;h=801ba1f499b566e677b763fc84f8ba86f4f7ccd0;hb=HEAD#l1059)
it defines the header/footer on every chunk to indicate that if it is allocated
and what size it is. With this information I threw together a python tool that
would reformat the heap into a human readable chunks showing what memory is
allocated and what is free in addition to it's contents.


With this I could then compare what allocations were added between core
snapshots. Some of the new allocations would be ok but since something was
leaking eventually the majority of the additions should be the leaked memory.
Luckily there was a very easy to identify allocation that kept showing up. This
allocation was a 20 byte allocation for a string that happened to be an error
message. There was an additional allocation that was just as present but less
obvious. This one was 36 bytes. The contents always had the same pointer as the
first entry followed by another pointer, which after investigations always
pointed to the heap, then the value 0x00000013 twice. The rest of it appeared to
uninitialized garbage. The first pointer was the key to understanding this
allocation. It happend to point to a vtable for an error object and the second
pointer pointed to an instance of the string that was the other allocation that
appeared to be leaking.


Having identified what type of objects were leaking I had to next find where
they were leaking from. The most obvious path that they might have come from
didn't look like it should leak and when I added a break point at that location
it was never hit even though the process continued to leak. Since the process handles
lots of data that is passed over IPC the next step was to trace the messages
going through the process. Since this is our own internal IPC we don't have nice
things like dbus-monitor so I had to use strace instead. I had considered
writing a conversion app that would parse out the strace output and put it into
human readable object form but it turns out I could get enough info by just
reading the strings in the messages without the encoded object information.
With strace I monitored all of the read and send calls handled by the process
and could then see where the message found in the heap had been passed through
the bus. By getting the context in which those messages were sent by looking at
preceeding transactions I could narrow in on the code that was in fact leaking.


Having narrowed in on the bad code it was soon fairly obvious why it was
leaking and from this I was able to get a more reproducible scenario to test
things out with. The reason for the leak was that we allow for observers to be
registered for objects even if they aren't on the bus. When the object in
question arrives on the bus if there are observers already present it will query
the object and save away some information with respect to that event. This
information was the information being leaked. The twist is that when the object
is removed from the bus that information is not freed up. Then if the same
object arrives on the bus again the process is repeated except the original
information is replaced, leaving it with no further references.


Having found the leak it was simply of matter of accounting for freeing the
memory when it was no longer needed.
