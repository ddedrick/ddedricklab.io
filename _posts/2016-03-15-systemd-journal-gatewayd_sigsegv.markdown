---
layout: post
title: "systemd-journal-gatewayd SIGSEGV"
comments: true
---
Our test shop caught an issue where systemd-journal-gatewayd crashed with a SEGV. The
core dump revealed the crash was in libmicrohttpd code in modifying a list. The
direct code path didn't make sense for the crash as we are checking if the list
has any items and if it does then we access one of the items. The item we
access ends up being NULL. We had orginally checked to see if there was a
previous item in the list and if so then we go and access the next item. This
should work because the list is doubly linked so if there is a previous there
should be a next, even if it is the same as the previous.


The issue though is that this list was not protected against multiple threads
adding to it. So there was another thread adding an item to the list and mid-add
we come in and try to add another. The scenario that made this occur is that 2
connections to the journal-gatewayd came in at nearly the same time and each
thread just happened to hit the list modification at the same time. In testing we
really should only have one connection per-machine however someone had added
this machine to the logging mechanism twice so it got 2 connections.


The obvious solution to this problem was to add locking around this unprotected
list, as I could see that it was modified on different threads without
protection. I went and implemented this and submitted it upstream but also noted
that in our case we should have skipped over this list since it wasn't used in
the mode we use. The upstream response, after much explaination of the issue,
was instead to condition the list modification for epoll only mode which was the
only place it was needed and already noted that it didn't work in the
multi-threaded mode that we had a failure in.


Upstream Mailing List Thread:

[https://lists.gnu.org/archive/html/libmicrohttpd/2016-03/msg00023.html](https://lists.gnu.org/archive/html/libmicrohttpd/2016-03/msg00023.html)
