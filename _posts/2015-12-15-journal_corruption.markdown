---
layout: post
title: "Journal corruption"
comments: true
---
For some time now we have been having issues with the journal getting corrupted
in a way that prevents further messages from being printed until the corruption
is either flushed or manually removed.

One common trait when looking at this corruption in the journal was the flags value
is always 0x97. The flags value should only ever be 0, 1, or 2. These flags
indicate what the compression value is for the object. 0x97 isn't a valid value.

The code that sets the flags should return 1 or 2 when compression passes and
when it decides not to compress it should be 0. However when examining the code
closer it can also return errors. If you convert 0x97 to signed decimal it translates
to -105, which is -ENOBUFS. -ENOBUFS is returned when the compression fails or
produces a larger object. The fix is to only set compression if the compression
code returned a valid value. Knowing this looking upstream I found that a fix
for this was already in place, although it wasn't when this issue first started
occuring for us.

Upstream fix:

[https://github.com/systemd/systemd/pull/1663](https://github.com/systemd/systemd/pull/1663)
