---
layout: post
title: "atfork breakage"
---
While upgrading poky I ran into unit test breakage with one of our more complex
peices of code. Attaching gdb showed that it was in `pthread_atfork()` waiting
on a lock. Inspecting the backtrace indicated that we had been in a fork handler
at the time and were attempting to reregister the function with
`pthread_atfork()`.


Digging into the relevant glibc code I discovered that the handler was called
with the same lock that `pthread_atfork()` held. So we were stuck in a deadlock
because the lock that `pthread_atfork()` needed would never be released.
Tracking back in glibc history I found that there had in fact been a change in
this path that was causing our issue. The relevant commit was [this one](https://sourceware.org/git/?p=glibc.git;a=commit;h=27761a1042daf01987e7d79636d0c41511c6df3c).


Just to be sure that the issue hadn't been fixed with a more recent version of
glibc I wrote the simplest test program to reproduce the issue. My initial
attempt failed on the version of glibc that was present on my Fedora system and
poking at the source I found that there was an additional conditional added to
the locking in the newer versions of glibc where it wouldn't bother if there was
only one thread. With this knowledge I added an additional thread after which I
was able to reproduce it.


Now I was left with the dilema of how to register the fork handler from the fork
handler. It turns out that this was a non-problem. I assume the original author
of this code assumed that after the fork completed the child wouldn't inherit
the fork handlers. This however is not true. The fork handlers remain present
and therefore reregistering them was pointless. Moving the `pthread_atfork()`
to just outside of this function cleared things up and the issue went away. I'm
constantly surprised at how many issues we run into due to glibc upgrades but it
must just be due to the weird edge case features of glibc that this legacy code
uses.
