---
layout: post
title: "RPM size issue"
comments: true
---
After integrating the latest poky update (pyro) some users reported
issues with generating images. The issue was that dnf was complaining that there
wasn't enough space on their system for the rpms that were going to be applied
to the rootfs. As it happens they had plenty of space left on the partition that
the rootfs would be written to. However they happened to be very tight on space
on their actual root filesystem.


It seemed obvious that something in dnf or rpm was looking at "/" and not the
actual destination. I could tell it was going to take a while for me to
understand how dnf and rpm all fit together and track down where the actual
issue was so I provided a work around to get people working while I got down to
the root cause. My work around was to just ignore any space issues that rpm
found.


In order to reproduce this issues I had to do a few things. The first was I had
to find the dnf command that was being run and the environment it was being run
in. To do this I ran bitbake under strace. I used that output in conjunction
with my newly created python script that will re-exec a command from strace
output preserving the environment. Additionally the command was run under pseudo
so I had to run an instance of pseudo. Finally I had to simulate a full root
partition which I did by creating a dummy file that just took up almost all of
the remaining space on my partition. With all of this I could rerun the dnf
command and reproduce the failure instantly.


Poking around lead me to librpm's space calculations. The calculations on the
surface seemed to always operate on the path in the rpm and not the destination
path. This didn't make sense to me since I know that you can use dnf to install
to a different rootfs. So I modified the code based on something else I had
seen to check to see if we had done a chroot. If we hadn't done it I would
prepend the actual destination rootfs. This still didn't fix the issue though.
With strace I could see that we were doing the statfs call on "/" and not the
correct location. This lead me to check the variable that was used to track if a
chroot had been done. When I did this it indicated that the chroot had been done
but when I looked at the strace no chroot had been done.


At this point I had looked upstreaming in dnf and rpm and found nothing that
would seem to fix this. I decided that this issue must still be a problem
upstream. So I went and tried to reproduce it with my local dnf installing to
directory in my home account. It however did not have any issues. Now I did an
strace on it and found that before it did the statfs it had done a chroot. This
chroot was critical. So why was this one doing the chroot and the other not?


So I added a number of debug statements around the chroot and oddly enough I see
that we called the chroot and continued on despite strace not seeing a chroot. I
then wrote a simple C test program that would chroot and do a statfs. In doing
so I found that chroot failed and the man pages indicated that was because only
root can do a chroot. This got me thinking that the chroot was failing because
we weren't root but we just continue on anyway. This didn't add up though
because there still was no chroot in the strace of the failing case.


Eventually I realized that in psuedo we were LD_PRELOADing libpseudo.so. I used
readelf to dump the symbols of libspeudo and sure enough it provided a copy of
chroot. After digging into pseudo I found that pseudo faked out chroot and made
wrappers for other glibc calls that used path so that it could fix them up. The
catch was that statfs, or more accurately in this case statvfs, was not one of
these calls. The solution then was to add statvfs as a wrapped call, which fixed
the issue. I sumbitted my changes to fix this to upstream psuedo.


Upstream Fix:

[http://git.yoctoproject.org/cgit.cgi/pseudo/commit/?id=5a8a8856a5d77cda914cd6682e0f4ac845cefc69](http://git.yoctoproject.org/cgit.cgi/pseudo/commit/?id=5a8a8856a5d77cda914cd6682e0f4ac845cefc69)

2018 edit: I'm not sharing my strace tool for running commands from a log at
[https://github.com/dandedrick/straceexec](https://github.com/dandedrick/straceexec).
You can install it with pip like this: ```pip install straceexec```
