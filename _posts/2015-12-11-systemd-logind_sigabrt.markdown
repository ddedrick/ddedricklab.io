---
layout: post
title: "systemd-logind SIGABRT"
comments: true
---
Automated tests were randomly failing with an extra unexpected crash. The journal
indicated that systemd-logind died with a SIGABRT. Downloading the sdk and
running gdb indicates that the SIGABRT happened in ```epoll_wait()``` syscall. This
didn't make sense initially since an abort would have been in the kill syscall.


I decided to investigate where the SIGABRT signal was coming from. By running ftrace
and tracing both signal and the kill syscall while streaming it I found that
systemd (pid 1) was the one sending the SIGABRT kill to systemd-logind. But
why?


Seaching the systemd source reveals that systemd will send a SIGABRT when
a service uses the software watchdog and the watchdog times out. Going back to
the journal reveals a message indicating that the software watchdog timed out of
systemd-logind.


Ok but now why did systemd-logind not respond for a whole
minute. Going back to the ftrace I found that a SIGSTOP was sent to
systemd-logind and exactly one minute later systemd-logind gets killed by the
watchdog. Once again, why?


A debug collection process was sending a SIGSTOP to all processes
that had /dev/events/input0 open. Removing sending SIGSTOP in this case works
since /dev/events/input0 doesn't actually need to have exclusive access in this
case anymore (if it ever did).


Commands:
~~~ bash
# list system calls that can be traced
trace-cmd list -e syscalls:
# add the following to the trace-start.conf to enable tracing the kill syscall
-e syscalls:sys_exit_kill
~~~
