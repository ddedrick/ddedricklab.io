---
layout: post
title: "A Simple Problem"
comments: true
---
Sometimes what looks like a hard problem is actually a really simple problem in
disguise. Unfortunately it ocassionally takes a bit longer to figure that out.


In the process of trying to prepare us to upgrade from 20+ year old jpeg to a
modern version I was stripping out our hacks from the ancient version. Doing so
will prevent people from adding new uses of the hacks. This is something that
has been a problem since I last worked on this. In doing so I removed what
appeared to be a completely unused variable from the jpeg structures. The result
was that everything compiled just fine but unit tests failed. This was
especially confusing because it appeared to be completely unused and the
compiler seemed to confirm this for me.


My first suspicion was that there was a hidden alignment requirement in some
piece of code that was behaving differently due to the change in the size of the
structure. If I added a variable of the same size back in with a different name
everything seemed to work. I tried to pad the structure by different alignment
sizes but they always seemed to fail unless the size was exactly the same as it
was before my removal. I tried removing other variables from the structure and
the failures still showed up.


Finally it occurred to me to check to make sure that the compile task for jpeg
was actually running. It did run so I looked at the log. Looking at the log
showed that make didn't think there was anything new to do. The problem was that
I had been changing the header file only and this specific build system wasn't
smart enough to know that changing this header meant that all the users of it
also needed to be rebuilt. So the header definition of the structure was out of
sync with what was built into the library. Forcing a rebuild of the library
fixed up the unit tests.
