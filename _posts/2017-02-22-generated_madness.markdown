---
layout: post
title: "Generated Madness"
comments: true
---
While analysing code growth between releases I discovered that the an
executable had grown from 3MB to 13MB in one release. Since this accounts for a
400% growth I decided it was at least worth investigating even if it wasn't a
massive impact to filesystem size. My first quick stab at discovering why was
to use readelf. I dumped the section information and discovered that it was the
.text section the grew. Meaning that 10MB of code was added. I then dumped the
symbols from before and after and diffed them. This showed me that almost 5,000
symbols were added in the one release. What is even more telling is that all of
the symbols began with the word "generated".


Knowing now that I was looking for something that would generate code with
symbols conforming to that naming convention I was able to narrow down in the
code where the issue was introduced. I also took this opportunity to take a
look at the code that was generated. To start with the .cpp file was 118,099
lines long. This in itself is a significant burden on the compiler. In this file
there are a whole lot of functions that look extremely similar and then one
function that is just a giant chain of if(strcmp(...)==0). This chain was 4000+
cases deep. This means that if the case you were looking for happened to be the
last one you would have to exercise every single one of those strcmp checks.
What I realised later was that it was even worse than that because this function
was called for all nodes first but only handled internal nodes. To put it simply
if you had an external node it would always run every single strcmp, fail to
find anything and then go look in a different function that handled external
nodes. So not only was this code wasting space it was also horribly inefficient.


Now to fix it. If we were only looking at one aspect at a time we might go a
number of different ways. Specifically if we were looking to fix the strcmp
inefficiency we might just replace it with a hash table of some sort. Another
factor we have to keep in mind as well is that the code was modified to be generated
to not keep a full json data structure, parsed from a file, always resident in
memory. This is a reasonable goal just with a poor choice of solution. So the
approach that I took was to modify the data to conform to that access pattern.
In other words I would take the giant single json file and seperate it out into
individual json files for each component, a component being an object that is
of the granularity with which the code accesses it. Now the code could simply
use the string that it used as a key (filename) to access the json data, without
loading the whole json into memory. We also have a distinct advantage that since
we are using squashfs splitting a single large file into many smaller files has
little effect on size, unlike it may on other filesystems.
