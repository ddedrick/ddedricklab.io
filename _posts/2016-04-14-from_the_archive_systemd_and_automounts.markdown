---
layout: post
title: "From the archive: systemd and automounts"
comments: true
---
When we were in the early days of android and the systemd roll out we were
random boot early boot hangs. They weren't easy to catch and it was early in
development so we ignored them for a while but they didn't go away. Finally we
added debug to get an early shell and ran a boot test. When we caught one upon
poking around we could see that most things worked but the second you tried to
access anything on the /var/fs mount it locked up. Systemd claimed that both the
automount and the mount were started at this point. However looking at the
journal I found a message indicating that systemd got a request to mount but the
mount was already mounted.


So systemd got a mount request for this device from the kernel's automount but
it said the mount was already started so it just ignores it. The problem with
this is that it gives no feedback to the kernel in this case. So the kernel
assumes the automounter is still working on the mount and will block forever.
This is a bug in the systemd automount implementation that doesn't handle this
error case properly.


We need to dig deeper though to see why would it already be
mounted if the kernel is only now asking about the automount. The reason is
because of our crazy mount/automount setup and the way systemd handles
dependencies. When systemd sees a mount or automount that has a source or
destination coming from another mount file it will start that mount file
automatically.


What happens is that we should just be starting automounts and not their
undelying mount because the kernel should kick the mount. However the
automounts themselves have dependencies on mounts and thus we start a mount and
automount for the same thing. The automount doesn't know the mount had started
yet and tells the kernel to setup the automount. Then when the kernel requests
the mount the automount sees the mount being present and ignores the request.


The solution we went with to start was to not stack mounts on top of automount.
This prevents the dependencies and removes automount from the picture in the
specific case we had issues with. I also implemented a fix to send an ack to the
kernel is it requests amount that is already mounted. We still have this patch
around today but I can't reproduce this with confidence enough to know if the
issue still exists to submit it upstream.


2018 edit: This was eventually fixed upstream. I really should have submitted my
changes even though I was having issues reproducing them. The upstream fix is here:
[https://github.com/systemd/systemd/pull/5916](https://github.com/systemd/systemd/pull/5916)
