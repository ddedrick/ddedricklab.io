---
layout: post
title: "Backtrace hell"
comments: true
---
A coworker had discovered that calls to ```backtrace()``` were failing to print
any information. When I started to dig in I found that with my test app a call
to ```backtrace()``` also would not work. ```backtrace()``` for my test app was coming from
libc which was in turn just doing a dlopen of libgcc and using its unwinding
functions. libgcc only has one option for unwinding arm which is using the arm
unwind tables (.ARM.eidx). Unfortunately for this case we don't build in arm
unwind information for every function so this backtrace only works if the
function happens to have arm unwinding information. The unwind information that
we actually use for things like tbstack happens to come from the DWARF debug
information (.debug_frame).


libgcc temptingly does have some DWARF support for other architectures. So I
proceeded to bash my head against libgcc to attempt to get it to support DWARF
for arm. This gets ugly as the arm abi requires arm unwind support in addition
to the fact that trying to get libgcc to support DWARF for arm was a nightmare
anyway. So this breaks that path. I could have attempted to put support for
both arm and DWARD unwinding but it would have been a huge tear up and might
have had odd consequense on unwinding paths. So I abandonded that path with a
much bruised head.


My next attempt was to create a backtrace glue library that would provide the
same functions as libgcc but use libunwind under the covers to do the unwinding.
Thus the only change needed to libc was to change the name of the library that
was dlopened. This seemed like a simple path the should have fixed the problem.
I did get my library working and with my test app it seemed to work just fine.
But when I forced the original process to crash and attempt to print its
backtrace nothing was changed. Also it didn't seem to be printing out any
of the debug I had added down this path.


Now things get even more complicated. When I attempted to attach gdb to
to try to understand better why it isn't calling my code a SIGILL occurs.
If I start it without gdb it is fine but every time I start it
with gdb it gets a SIGILL.


Well sure I can work around that by starting it
normally and attaching gdb. At first it seems ok but then when I set a break
point and continue it crashes with a SIGILL. I poke around and find that a
number of other executable display the same behavior.


After poking around on google I found
https://sourceware.org/git/gitweb.cgi?p=binutils-gdb.git;a=commit;f=gdb/arm-tdep.c;h=d403db2720ef6ac091dd1c0101ffc60242199528
which fixes the gdb issue. As it turns out the common thread
between the executables that failed was the abi version which can be found using
readelf -h. The executables that are "UNIX - System V" worked fine but the ones
that were "UNIX - GNU" did not.


Ok gdb side track down I hook it up to proccess again. Now I can see exactly why
it isn't calling my backtrace code. The reason is that pagemaker links
libpthread in addition to libc and libpthread provides its own completely
different version of ```backtrace()```. Its version relies fully on frame pointers
being present and valid. At this point it would be too much of a tear up to hack
my library into libpthread which is significantly different to libc's
implementation.

So I fall back to the simple solution that I had actually considered and
dismissed at the beginning, which was to change the backtrace dumping code in question to use
libunwind instead of backtrace(). I had dismissed this option because it seemed
to me that backtrace() should simply work and if not it should be fixed. What I
had missed was the following information from the man page that excludes it from
working on our system:

```
       These functions make some assumptions about how a function's return
       address is stored on the stack.  Note the following:

       *  Omission of the frame pointers (as implied by any of gcc(1)'s nonzero
       *  optimization levels) may  cause  these  assump-
          tions to be violated.

       *  Inlined functions do not have stack frames.

       *  Tail-call optimization causes one stack frame to replace another.
```

I actually like the libc backtrace() interface better than libunwind's for the
simple case that this code used. linbunwind is more powerful though and works
in more cases, including ours. Additionally libunwind will
fallback to other methods if one fails. So first it will unwind with DWARF and
when it hits a dead end it will try the arm unwinding and when that fails it
goes to APCS frame. At this point I hope it is a very long time before I ever
have to take a look at gcc code again.
