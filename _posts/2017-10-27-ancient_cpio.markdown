---
layout: post
title: "Ancient cpio"
comments: true
---
Another issue popped up for one specific user after the poky upgrade. The issue
happened to be in the ```do_package()``` task for ui-android. The python script for
the ```do_package()``` task was calling out to a shell with a big piped together
command and the result was failing somewhere along the line. Unfortunately the
command was sending stderr to /dev/null so none of the error output was
captured.


The first step was that I logged onto the machine where the build occurred and
ran the specific command to see what the results were. However when I ran the
command it worked just fine and didn't return any errors. So I decided to try to
reproduce it on my machine. I pulled the exact same revision of of code and
kick a build. The result was that I didn't run into any issues. I'm wondering
now if maybe this is an issue specific to ubuntu so I log into an ubuntu machine
and repeat the process. My results were the same and I hit no issues.


At this point I'm quite confused as to how I can't hit this and decide to use
rsync to copy the entire failing workspace to my machine at the same location
and run it there. After the long rsync finished and I kicked a build I was able
to hit the issue. So there was something about the workspace that was actually
the problem but what?


Rather than mess with the command that was failing to remove the redirect of
stderr to /dev/null I decided to just strace the whole process and get a more
detailed look at what was going on. The output was huge but when I narrowed in
on the failing process I found that it was cpio and it was printing out that the
"-0" option was not supported. I checked on my machine though and cpio seemed to
state that -0 was a valid option. So I took a look at the path the execve used
in the strace and I found it. The path it was using was to an ancient build tools
path because in the workspace tmp/hosttools/cpio was
a symlink to it. In my workspaces though cpio was a symlink to /bin/cpio. So I
was able to conclude that the person who made the workspace had the mls ancient build tools path in
their PATH before /bin. The ancient version of cpio was so old it didn't
have this option and also wouldn't even print out a version.
